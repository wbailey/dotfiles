call plug#begin()
" https://github.com/junegunn/vim-plug
" The default plugin directory will be as follows:
"   - Vim (Linux/macOS): '~/.vim/plugged'
"   - Vim (Windows): '~/vimfiles/plugged'
"   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'

" PLUGINS
" useful vim-plug commands:
" :PlugInstall
" :PlugUpdate
" :PlugClean



" https://github.com/iamcco/markdown-preview.nvim
" Start the preview :MarkdownPreview
" Stop the preview :MarkdownPreviewStop
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && npx --yes yarn install' }
" https://docs.github.com/en/copilot/using-github-copilot/getting-started-with-github-copilot?tool=vimneovim
Plug 'github/copilot.vim'
" js quality of life
Plug 'pangloss/vim-javascript'
Plug 'styled-components/vim-styled-components', { 'branch': 'main' }
" ts QoL
Plug 'leafgarland/typescript-vim'
" code completion
" Plug 'valloric/youcompleteme'
" graphQl  jparise/vim-graphql
" colo schemes
Plug 'joshdick/onedark.vim'
" https://vimawesome.com/plugin/vim-colorschemes-sweeter-than-fiction
Plug 'flazz/vim-colorschemes'
" language support
Plug 'sheerun/vim-polyglot'
" c syntax highlighting
" needs extra config https://github.com/jeaye/color_coded (causing issues with coloring non c files)
" Plug 'jeaye/color_coded'
" Plug 'powerline/powerline'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'pantharshit00/vim-prisma'
" https://github.com/vim-airline/vim-airline/wiki/Screenshots
let  g:airline_theme='jellybeans'
" :CocInstall coc-json coc-tsserver
" https://github.com/neoclide/coc.nvim
" Use release branch (recommended)
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" https://github.com/jackguo380/vim-lsp-cxx-highlight (requires coc config mods)
" c syntax highlighint
" Plug 'jackguo380/vim-lsp-cxx-highlight'
" coc command :CocInstall, :CocConfig
let g:coc_global_extensions = [ 
	\ 'coc-clangd', 
	\ 'coc-tsserver',
 	\ 'coc-sh',
 	\ 'coc-css',
 	\ 'coc-jedi',
	\ 'coc-json',
	\ 'coc-prisma',
	\ 'coc-sql',
	\]
 
" 	\ 'coc-html',
if isdirectory('./node_modules') && isdirectory('./node_modules/prettier')
	let g:coc_global_extensions += ['coc-prettier']
endif

if isdirectory('./node_modules') && isdirectory('./node_modules/eslint')
	let g:coc_global_extensions += ['coc-eslint']
endif

" for coc-tsserver
augroup ReactFiletypes
  autocmd!
  autocmd BufRead,BufNewFile *.jsx set filetype=javascriptreact
  autocmd BufRead,BufNewFile *.tsx set filetype=typescriptreact
augroup END

" https://github.com/nvim-treesitter/nvim-treesitter
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}


call plug#end()
" copilot hotkey '\c' in normal mode 
let g:copilot_enabled = 0

function! ToggleCopilot()
  if g:copilot_enabled == 0
      :Copilot enable
      let g:copilot_enabled = 1
  else
      :Copilot disable
      let g:copilot_enabled = 0
  endif
endfunction

nnoremap <F2> :call ToggleCopilot()<CR>

" tree sitter config
lua << EOF
require'nvim-treesitter.configs'.setup {
  -- A list of parser names, or "all" (the five listed parsers should always be installed)
  ensure_installed = {
    "bash",
    "c",
	  "comment",
    "cmake",
    "cpp",
    "css",
    "dockerfile",
    "help",
    "html",
    "htmldjango",
    "javascript",
    "json",
    "make",
    "markdown",
    "markdown_inline",
    "meson",
    "ninja",
    "regex",
    "scss",
    "sql",
    "tsx",
    "typescript",
    "vim",
    "yaml"	
    },

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- Automatically install missing parsers when entering buffer
  -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
  auto_install = true,

  -- List of parsers to ignore installing (for "all")
  ignore_install = { },

  ---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
  -- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

  highlight = {
    enable = true,

    -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
    -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
    -- the name of the parser)
    -- list of language that will be disabled
    disable = { },
    -- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files
    -- disable = function(lang, buf)
    --     local max_filesize = 100 * 1024 -- 100 KB
    --     local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
    --     if ok and stats and stats.size > max_filesize then
    --         return true
    --     end
    -- end,

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
  indent = {
    enable = false
  }
}
EOF

" BASIC CONFIG

" set number	
set linebreak  
set showbreak=≡
set textwidth=0	
set showmatch	
" visual bell flashes the screen
set visualbell
set belloff=all
let python_highlight_all=1
syntax on
" all the pretty colors
" blue.vim
" darkblue.vim
" default.vim
" delek.vim
" desert.vim
" elflord.vim
" evening.vim
" koehler.vim
" morning.vim
" murphy.vim
" pablo.vim
" peachpuff.vim
" ron.vim
" shine.vim
" slate.vim
" torte.vim
" zellner.vim
" for custom themes, can be light
set background=dark 
colorscheme Benokai 
" stop newlines after comments from automatically being commented
set formatoptions-=cro
" set mouse to move panes, etc
" if !has('nvim'):
" 	set mouse=a
" 	if has("mouse_sgr")
" 		" for mac mouse
" 		set ttymouse=sgr
" 	else
" 		" for linux mouse
" 		set ttymouse=xterm2
" 	end
" endif
set hlsearch	
set smartcase	
set ignorecase	
set incsearch	
set autoindent
set ai
set si
" set shiftwidth=2
set smartindent	
set smarttab  
set softtabstop=2
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab
set ruler     
set undolevels=1000   
set backspace=indent,eol,start
" set noswapfile
" formatting file explorer https://shapeshed.com/vim-netrw/
let g:netrw_liststyle = 3

" LINE NUMBERS

set number

augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave,WinEnter * if &nu && mode() != "i" | set rnu   | endif
  autocmd BufLeave,FocusLost,InsertEnter,WinLeave   * if &nu                  | set nornu | endif
augroup END

" FILE TYPE FORMATING

if has("autocmd")
	" syntax highligh on large files (slow)
	" autocmd BufEnter *.{js,jsx,ts,tsx} :syntax sync fromstart
	" autocmd BufLeave *.{js,jsx,ts,tsx} :syntax sync clear
	" do not continue comments after a newline
	autocmd FileType * set formatoptions-=cro
    " Use filetype detection and file-based automatic indenting.
    filetype plugin indent on

    " Use actual tab chars in Makefiles.
    autocmd FileType make setlocal tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab
    " js and fam
    autocmd FileType javascript,html,css,javascriptreact,typescript,typescriptreact setlocal tabstop=4 softtabstop=4 shiftwidth=4
    " python
    autocmd FileType python setlocal tabstop=4 softtabstop=4 shiftwidth=4 textwidth=79 expandtab autoindent fileformat=unix encoding=utf-8
    " clang
    autocmd FileType *.c,*.h setlocal tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab colorcolumn=110 highlight ColorColumn ctermbg=darkgray
endif

" CUSTOM HOTKEYS

" NERDTress Ctrl+n
" map <C-n> :NERDTreeToggle<CR>

" Use ctrl-[hjkl] to select the active split!
" nmap <silent> <c-k> :wincmd k<CR>
" nmap <silent> <c-j> :wincmd j<CR>
" nmap <silent> <c-h> :wincmd h<CR>
" nmap <silent> <c-l> :wincmd l<CR>

" Commenting blocks of code.
" https://stackoverflow.com/questions/1676632/whats-a-quick-way-to-comment-uncomment-lines-in-vim
" type ,cc to comment a line and ,cu to uncomment a line (works both in normal and visual mode).
" augroup commenting_blocks_of_code
"   autocmd!
"   autocmd FileType c,cpp,java,scala let b:comment_leader = '// '
"   autocmd FileType sh,ruby,python   let b:comment_leader = '# '
"   autocmd FileType conf,fstab       let b:comment_leader = '# '
"   autocmd FileType tex              let b:comment_leader = '% '
"   autocmd FileType mail             let b:comment_leader = '> '
"   autocmd FileType vim              let b:comment_leader = '" '
" augroup END
" noremap <silent> ,cc :<C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
" noremap <silent> ,cu :<C-B>silent <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:nohlsearch<CR>
