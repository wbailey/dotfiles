#  dotfiles

various config files of mine

```shell
# run install script
sh -c "$(curl -fsSL https://gitlab.com/wbailey/dotfiles/-/raw/main/install.sh)"
```

```shell
# get vimrc
curl https://gitlab.com/wbailey/dotfiles/-/raw/main/.vimrc > ~/.vimrc
```

```shell
# get oh my zsh theme
curl https://gitlab.com/wbailey/dotfiles/-/raw/main/af-magic-custom.zsh-theme > ~/.oh-my-zsh/themes/af-magic-custom.zsh-theme
```